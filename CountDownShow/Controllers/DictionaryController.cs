﻿using CountDownShow.Models;
using CountDownShow.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CountDownShow.Controllers
{
    public class DictionaryController : Controller
    {
        private IDictionaryService _dictionaryService;
        private IScoreService _scoreService;
        Score _score;
        int _round;
        public DictionaryController(IDictionaryService dictionaryService, IScoreService scoreService)
        {
            _dictionaryService = dictionaryService;
            _scoreService = scoreService;
            _score = getScore().Result;
            _round = _score.Round;
        }
        public async Task<Score> getScore()
        {
            return await _scoreService.getSelf();
        }
        public async Task<IActionResult> Index(string welcome)
        {
            if (_score.Round != 5)
            {
                if (_score.Round == 6)
                {
                    _score.Round = 1;
                    _score.TotalScore = 0;
                }
                ViewBag.Title = "Count Down Game";
                _dictionaryService.getSelf().InputWord = "";
                _dictionaryService.getSelf().LongestWord = "";
                if (welcome == null)
                {
                    ViewBag.welcome = "Welcom Count Down Game,Please input at least 3 different vowels and 4 different consonants";
                }
                else
                {
                    ViewBag.welcome = welcome;
                }
                Dictionary dictionary = _dictionaryService.getSelf();
                return View(dictionary);
            }
            else {
                return RedirectToAction(nameof(Finish));
            }
        }

        //[HttpPost]
        public async Task<IActionResult> AddNineWords(Dictionary ninewords)
        {
            if (ModelState.IsValid)
            {
                await _dictionaryService.setNineWords(ninewords.NineWord);
            }
            if (_dictionaryService.checkNineWords())
            {
                return RedirectToAction(nameof(CountDown));
            }
            else 
            {
                return RedirectToAction(nameof(Index),new { welcome = "Please input at least 3 different vowels and 4 different consonants" });
            }
        }

        public IActionResult CountDown()
        {
            ViewBag.Title = "Count Down Game";
            ViewBag.text = "You have 30 seconds";
            ViewBag.nineWord = _dictionaryService.getNineWords().ToString().ToUpper();
            Dictionary dictionary = _dictionaryService.getSelf();
            return View(dictionary);
        }

        public IActionResult Finish()
        {
            ViewBag.Title = "Count Down Game";
            ViewBag.TotalScore = "Game Over. Your total score is " + _score.TotalScore;
            return View();
        }

        public async Task<IActionResult> InputAnswer(Dictionary answer)
        {
            if (ModelState.IsValid)
            {
                _dictionaryService.setInputWord(answer.InputWord);
                await _dictionaryService.setLongestWord();
                return RedirectToAction(nameof(Result));
            }
            else {
                return RedirectToAction(nameof(CountDown));
            }
        }

        public IActionResult Result()
        {
            ViewBag.Title = "Count Down Game";
            if (_dictionaryService.checkInputWord())
            {
                ViewBag.answer = "Good job, your answer is " + _dictionaryService.getSelf().InputWord;
                if (_dictionaryService.getSelf().InputWord.Length == 9)
                {
                    _score.RoundScore = 18;
                }
                else
                {
                    _score.RoundScore = _dictionaryService.getSelf().InputWord.Length;
                }
            }
            else {
                ViewBag.answer = "Failed, your answer " + _dictionaryService.getSelf().InputWord + " isn't a  word or not belong the Nine word";
                _score.RoundScore = 0;
            }
            ViewBag.longestWord = "The Longest word is " + _dictionaryService.getSelf().LongestWord;
            ViewBag.roundScore = "This round score is " + _score.RoundScore;
            _scoreService.AddScore();
            Dictionary dictionary = _dictionaryService.getSelf();
            return View(dictionary);
        }

        public IActionResult NextRound()
        {
            _score.Round = _score.Round + 1;
            return RedirectToAction(nameof(Index));
            
        }
    }
}
