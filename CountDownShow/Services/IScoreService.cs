﻿using CountDownShow.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CountDownShow.Services
{
    public interface IScoreService
    {
        public void AddScore();

        public Task<Score> getSelf();
    }
}
