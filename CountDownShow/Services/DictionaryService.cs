﻿using CountDownShow.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace CountDownShow.Services
{
    public class DictionaryService : IDictionaryService
    {
        protected Dictionary _dictionary = new Dictionary();

        public DictionaryService()
        {
            getAllWords();
        }
        public bool checkInputWord()
        {
            string inputWord;
            try
            {
                 inputWord = _dictionary.InputWord.ToLower();
            }
            catch {
                return false;
            }
            //if input word belong to nineWords
            char[] nineWords = _dictionary.NineWord.ToLower().ToCharArray();
            ArrayList nineWordslist = new ArrayList(nineWords);
            char[] words = _dictionary.InputWord.ToLower().ToCharArray();
            ArrayList inputWords = new ArrayList(words);
            int length = 0;
            for (int i = 0; i < inputWords.Count; i++)
            {
                for (int j = 0; j < nineWordslist.Count; j++)
                    {
                    if (nineWordslist[j].ToString() == inputWords[i].ToString())
                    {
                            nineWordslist[j] = "1";
                            inputWords[i] = "0";
                            length++;
                    }
                }
            }

            if(length != inputWords.Count)
            {
                return false;
            }

            //if input word belong to dictionary
            foreach (string word in _dictionary.AllWords)
            {
                if (inputWord == word) {
                    return true;
                }
            }
            return false;
        }

        public bool checkNineWords()
        {
            string[] vowels = { "A", "E", "I", "O", "U" };
            int vowelsNum = 0;
            char[] nineWords = _dictionary.NineWord.ToUpper().ToCharArray();
            var result = true;
            //check 3 vowels and 4 consonants
            for (int i = 0; i < nineWords.Length; i++) 
            {
                //check 3 vowels and 4 consonants
                for (int v = 0; v < vowels.Length; v++)
                {
                    if (nineWords[i].ToString() == vowels[v])
                    {
                        vowelsNum++;
                    }
                }
            }
            if (vowelsNum < 3 || vowelsNum > 5) {
                result = false;
            }

            //check length
            if (nineWords.Length != 9) {
                result = false;
            }
            return result;
        }

        public int countScore(string inputWord)
        {
            throw new NotImplementedException();
        }

        public void getAllWords()
        {
            string line;
            ArrayList list = new ArrayList();
            StreamReader file = new StreamReader("dictionary.txt");
            while ((line = file.ReadLine()) != null)
            {
                list.Add(line);
            };
            _dictionary.AllWords = list;
        }

        public string getNineWords()
        {
            return _dictionary.NineWord;
        }

        public Dictionary getSelf()
        {
            return _dictionary;
        }

        public void setInputWord(string inputWord)
        {
            _dictionary.InputWord = inputWord;
        }

        public Task setLongestWord()
        {
            char[] nineWords = _dictionary.NineWord.ToLower().ToCharArray();
            string longestWord = "a";
            foreach (string word in _dictionary.AllWords)
            {
                ArrayList nineWordslist = new ArrayList(nineWords);
                char[] words = word.ToLower().ToCharArray();
                ArrayList dicWordslist = new ArrayList(words);
                int length = 0;
                for (int i = 0; i < dicWordslist.Count; i++)
                {
                    for (int j = 0; j < nineWordslist.Count; j++)
                    {
                        if (nineWordslist[j].ToString() == dicWordslist[i].ToString())
                        {
                            nineWordslist[j] = "1";
                            dicWordslist[i] = "0";
                            length++;
                        }
                    }
                }
                if ((length == dicWordslist.Count) && (length > longestWord.Length)) {
                    longestWord = word;
                }
            }
            _dictionary.LongestWord = longestWord;
            Console.WriteLine(_dictionary.LongestWord);

            return Task.CompletedTask;
        }

        public Task setNineWords(string nineWord)
        {
            _dictionary.NineWord = nineWord;
            return Task.CompletedTask;
        }
    }
}
