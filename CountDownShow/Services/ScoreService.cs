﻿using CountDownShow.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CountDownShow.Services
{
    public class ScoreService : IScoreService
    {
        protected Score _score = new Score();
        public ScoreService()
        {
            _score.Round = 1;
            _score.TotalScore = 0;
        }
        public void AddScore()
        {
            _score.TotalScore += _score.RoundScore;
        }
        public Task<Score> getSelf()
        {
            return Task.Run(function: () => {
                return _score;
            });
        }
    }
}
