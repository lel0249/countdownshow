﻿using CountDownShow.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CountDownShow.Services
{
    public interface IDictionaryService
    {
        public void getAllWords();
        public bool checkNineWords();
        public bool checkInputWord();
        Task setLongestWord();
        public int countScore(string inputWord);
        Task setNineWords(string nineWord);
        public string getNineWords();
        public Dictionary getSelf();
        public void setInputWord(string inputWord);
    }
}
