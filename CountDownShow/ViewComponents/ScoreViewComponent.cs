﻿using CountDownShow.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CountDownShow.ViewComponents
{
    public class ScoreViewComponent:ViewComponent
    {
        private readonly IScoreService _scoreService;
        public ScoreViewComponent(IScoreService scoreService)
        {
            _scoreService = scoreService;
        }
        public async Task<IViewComponentResult> InvokeAsync()
        {
            var score = await _scoreService.getSelf();
            return View(score);
        }
    }
}
