﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CountDownShow.Models
{
    public class Dictionary
    {
        public ArrayList AllWords { get; set; }

        public string NineWord { get; set; }

        public string LongestWord { get; set; }

        public string InputWord { get; set; }
    }
}
