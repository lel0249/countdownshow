﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CountDownShow.Models
{
    public class Score
    {
        public int TotalScore { get; set; }

        public int RoundScore { get; set; }

        public int Round { get; set; }
    }
}
